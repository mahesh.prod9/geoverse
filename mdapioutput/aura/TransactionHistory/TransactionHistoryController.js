({
    doInit : function(component, event, helper) {
        component.set('v.mycolumns', [
            {label: 'Name', fieldName: 'Name', type: 'text'},
            {label: 'Amount', fieldName: 'npe01__Payment_Amount__c', type: 'text'},
            {label: 'Method', fieldName: 'npe01__Payment_Method__c', type: 'text'},
            {label: 'Date', fieldName: 'npe01__Payment_Date__c', type: 'text'}
        ]);
        var action = component.get("c.getDetails");
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('state '+state);
            if(state === 'SUCCESS'){
                console.log('method invoked');
                var result = response.getReturnValue();
                console.log('Payments',result);
                component.set("v.payments",result);
            }
        });
        $A.enqueueAction(action);    
    }
    
})