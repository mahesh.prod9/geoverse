({
	handleCreateOpportunity: function(component, event,helper) {
         component.set('v.isLoading',true);
            var action = component.get("c.createOpportunity");
         console.log(JSON.parse(JSON.stringify(component.get("v.newOpp"))));
        action.setParams({
            "opportunityobj": JSON.stringify(component.get("v.newOpp"))
        });
        //console.log('**'+component.get("v.newOpp"));
        // Configure the response handler for the action
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state', state);
            if(state === "SUCCESS") {
                console.log("Opportunity created successfully");
                         component.set('v.isLoading',false);
                component.set('v.message',true);
                
                //component.set("v.isVisible", false);
            }
            else if (state === "ERROR") {
                console.log('Problem saving Opportunity, response state: ' + state);
            }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
        });
        
        // Send the request to Opportunity the new opportunity
        $A.enqueueAction(action);
     
    },
    closeModel: function(component,event,helper){
         window.open("https://mouritech--yicrmdev.lightning.force.com/lightning/n/Donations","_self");
    }
})