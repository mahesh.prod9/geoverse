({
	OnClickProfileHandler : function(component, event, helper) {
		component.set("v.truthy", true);
        component.set("v.IsHomeVisible", false);
	},
    OnclickHomeTabHandler : function(component, event, helper) {
		component.set("v.truthy", false);
        component.set("v.IsHomeVisible", true);
	},
})