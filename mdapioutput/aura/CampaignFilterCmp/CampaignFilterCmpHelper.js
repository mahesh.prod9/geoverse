({
	getDetails : function(component,event) {
		
         var action =  component.get("c.getStatus");
                       
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.status", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
        component.set('v.columns', [
                {label: 'Campaign Name', fieldName: 'Name', type: 'text'},
                {label: 'Description', fieldName: 'Description__c', type: 'Long Text Area'},
                {label: 'Start Date', fieldName: 'Start Date', type: 'Date'}
            ]);
        
         component.set('v.columns_one', [
                {label: 'Campaign Name', fieldName: 'Name', type: 'text'},
                {label: 'Description', fieldName: 'Description__c', type: 'Long Text Area'},
                {label: 'Start Date', fieldName: 'Start Date', type: 'Date'}
            ]);
        
        var action =  component.get("c.getCmpInProgress");
                       
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log(response.getReturnValue());
                component.set("v.data_progress", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
        var action =  component.get("c.getCmpPlanned");
                       
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                console.log(response.getReturnValue());
                component.set("v.data_planned", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	}
})