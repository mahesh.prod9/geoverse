({
	doInit : function(component, event, helper) {
     helper.getDetails(component,event);        
	},  
    
    doSearch : function(component, event, helper) {
     
        
         component.set('v.resultColumns', [
                {label: 'Campaign Name', fieldName: 'Name', type: 'text'},
                {label: 'Status', fieldName: 'Status', type: 'text'},
                {label: 'Description', fieldName: 'Description__c', type: 'Long Text Area'},
                {label: 'Start Date', fieldName: 'StartDate', type: 'Date'}
            ]);
        
         
        var action =  component.get("c.getSelectedCampaign");
        
       /* alert(component.get("v.selectedStatus"));
        alert(component.get("v.cmpName"));
        alert(component.get("v.startDate"));*/
    	
         action.setParams({status: component.get("v.selectedStatus"), name : component.get("v.cmpName"),startDate: component.get("v.startDate")});
     
         action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
              component.set("v.resultData",response.getReturnValue());
              component.set("v.isVisible", false);
            }
        });
        $A.enqueueAction(action);
	},
    
    doRefresh : function(component, event, helper) {
     window.location.reload(false);  
    },
})