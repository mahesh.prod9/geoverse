({
	doInit : function(component, event, helper) {  
         component.set('v.mycolumns', [
                
                {label: 'Related Contact', fieldName: 'RelatedContact', type: 'text'},
                {label: 'Type', fieldName: 'type', type: 'text'},
                 {label: 'Status', fieldName: 'status', type: 'text'},
              {label: 'Description', fieldName: 'Description', type: 'text'},
             {label: 'Contact address', fieldName: 'Address', type: 'text'}
            ]);
        var action = component.get('c.getMembers');
        
        action.setCallback(this,function(response){
         var state = response.getState();
            if(state === 'SUCCESS'){
                var returnValue = response.getReturnValue();
                console.log('ret value is'+returnValue);
                component.set("v.MembersList",returnValue);
            }
            
        });
        
        $A.enqueueAction(action);		
	}
})