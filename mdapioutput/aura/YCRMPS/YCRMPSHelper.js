({
    /* For all tabs, remove Classes that make the tab visible and 
     * add Class to hide the tab
     */
    deactivateAllTabs: function(component, event, helper) {
        // When adding a new tab, the tabname must be added to the allTabs array:
        var allTabs = [
            'home',
            'accounts',
            'example',
            'option1',
            'option2',
            'option3'
        ]
        // Hide the dropdown on Example menu:
        $A.util.removeClass(component.find("menuExample"), 'slds-is-open');
        // Deactivate all tabs:
        allTabs.forEach(deactivate);
        function deactivate(item) {
            var tabToDeactivate = component.find(item + '-tab-auraid');
            var contentToHide = component.find(item + '-content-auraid');
            $A.util.removeClass(tabToDeactivate, 'slds-active');
            $A.util.removeClass(tabToDeactivate, 'slds-has-focus');
            $A.util.removeClass(tabToDeactivate, 'tabActive');
            $A.util.removeClass(contentToHide, 'slds-show');
            $A.util.addClass(contentToHide, 'slds-hide');
        }
    },
    /* For the active tab, add Classes to make the tab visible and 
     * remove Class that hides the tab
     */
    activateTab: function(component, event, helper, tab) {
        var tabToActivate = component.find(tab + '-tab-auraid');
        var contentToShow = component.find(tab + '-content-auraid');
        $A.util.addClass(tabToActivate, 'slds-active');
        $A.util.addClass(contentToShow, 'slds-show');
        $A.util.removeClass(contentToShow, 'slds-hide');
        // If it's an overflow tab we need to activate the parent:
        var overflowTabs = [
            'option1',
            'option2',
            'option3'
        ];
        if (overflowTabs.includes(tab)) {
            $A.util.addClass(component.find('example-tab-auraid'), 'slds-has-focus');
            $A.util.addClass(component.find('example-tab-auraid'), 'tabActive');
        }
    },
})