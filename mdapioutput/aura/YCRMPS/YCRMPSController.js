({
    switchTabs: function(component, event, helper) {
        /* Use html5 data attributes to pass the tab name as the
         * data-tab attribute in the onclick event in component
         */
        var tab = event.currentTarget.dataset.tab;
        component.set("v.activeTab", tab);
        helper.deactivateAllTabs(component, event, helper);
        helper.activateTab(component, event, helper, tab);
    },
    toggleExampleMenu: function(component, event, helper) {
        var exampleMenu = component.find("menuExample");
        $A.util.toggleClass(exampleMenu, 'slds-is-open');
    },
    toggleExampleMenu1: function(component, event, helper) {
        var exampleMenu = component.find("menuExample1");
        $A.util.toggleClass(exampleMenu, 'slds-is-open');
    },
    login:function(component,event,helper){
       window.open('https://yicrmdev-sampleeinsteinbot.cs97.force.com/fundingprograms/s/login/?ec=302&startURL=%2Ffundingprograms%2Fs%2F'); 
    },
    signup:function(component,event,helper){
         window.open('https://mouritech--yicrmdev--c.visualforce.com/apex/createContactpage'); 
    },
    doOnclick:function(component,event,helper){
        component.set("v.isDisplayed", false);
    },
    navigateDonation:function(component,event,helper){
        
         window.open('https://mouritech--yicrmdev--c.visualforce.com/apex/NewDonations');
    }
})