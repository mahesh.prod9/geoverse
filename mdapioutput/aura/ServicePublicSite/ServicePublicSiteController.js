({
    createServiceParticipant:function(component,event,helper){
         component.set('v.isDisplay',true);  
        window.open("https://yicrmdev-sampleeinsteinbot.cs97.force.com/fundingprograms/s/","_blank");       
     	
     },
    handleCreateServiceSchedule: function(component, event,helper) {
        var action = component.get("c.createServiceSchedulerec");
        component.set('v.ServiceId',component.get('v.newService.Id'));
            action.setParams({
                "idService" : "a1a0U000001MrL9QAK" 
            });
        console.log(component.get("v.ServiceId"));
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.message", "ServiceSchedule created successfully");
                component.set("v.isDisplay",true);
            }
            else if (state === "ERROR") {
                console.log('Problem saving ServiceSchedule, response state: ' + state);
            }
            else {
                console.log('Unknown problem, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    showSpinner: function(component, event, helper) { 
        component.set("v.Spinner", true); 
   },
     hideSpinner : function(component,event,helper){  
       component.set("v.Spinner", false);
    },
    closeModel : function(component,event,helper){
        component.set('v.isDisplay',false);
        //window.open('https://yicrmdev-sampleeinsteinbot.cs97.force.com/fundingprograms/s/program/pmdm__Program__c/00B0U000004lKsTUAU','_self');
    }
})