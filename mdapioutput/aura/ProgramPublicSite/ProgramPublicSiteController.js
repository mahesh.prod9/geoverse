({
	doInit : function(component, event, helper) {
        
	  var action = component.get('c.InitMethod');
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.wrapperList",response.getReturnValue());
                console.log(response.getReturnValue());
            }
        });
        
        $A.enqueueAction(action);	
	}
})