({
   doInit : function(component, event, helper) {
        var action = component.get("c.fetchUserDetail");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                component.set('v.oUser', res);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);	
       
       var action = component.get("c.getContact");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.contact", response.getReturnValue());
                console.log(response.getReturnValue());
            }
         });
         $A.enqueueAction(action); 
    },
    
    handleChange: function(cmp, event, helper) {
        var firstname = cmp.find("firstname").get("v.value");
        var lastname = cmp.find("lastname").get("v.value");
    },
    
    
    handleEditClick : function (cmp, event, helper) {
        cmp.set("v.edit", false);
        cmp.set("v.back", true);
	},
    handleBackClick : function(component, event, helper) {
		component.set("v.edit", true);
        component.set("v.back", false);
	},
})