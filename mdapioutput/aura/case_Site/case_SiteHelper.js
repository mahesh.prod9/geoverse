({
    fetchCasHelper : function(component, event, helper) {
        component.set('v.mycolumns', [
                {label: 'Case Number', fieldName: 'CaseNumber', type: 'String'},
                {label: 'Status', fieldName: 'Status', type: 'Picklist'},
                {label: 'Type', fieldName: 'Type', type: 'Picklist'}
            ]);
        var action = component.get("c.fetchCase");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.casList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})