({
	doInit : function(component, event, helper) {
		var action = component.get("c.getAccountIds");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                alert("res" + res);
                component.set('v.AccIdsList', res);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
	},
    
    countAccounts : function(component, event, helper) {
        console.log('------> Received Event. In countAccounts() method!');
        var isSelected = event.getParam("selected");
        var currentCount = component.get("v.count");
        if(isSelected)
            currentCount++;
        else
            currentCount--;
        
        if(currentCount <0)
            currentCount = 0;
        
        component.set("v.count", currentCount);
    }
})