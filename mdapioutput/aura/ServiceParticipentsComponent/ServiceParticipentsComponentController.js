({
    myAction : function(component, event, helper) 
    {
        component.set('v.columns', [
            {label: 'Participent name', fieldName: 'Name', type: 'text'},
            {label: 'Date', fieldName: 'pmdm__SignUpDate__c', type: 'Date'},
            {label: 'Status', fieldName: 'pmdm__Status__c', type: 'Picklist'}
        ]); 
        var SpList = component.get("c.servicePartcpnt");
        SpList.setParams
        ({
            scheduleId: component.get("v.ServiceScheduleId")
        });
        
        SpList.setCallback(this, function(data) 
                           {
                               component.set("v.ServiceParticipentList", data.getReturnValue());
                           });
        $A.enqueueAction(SpList);
    }
})