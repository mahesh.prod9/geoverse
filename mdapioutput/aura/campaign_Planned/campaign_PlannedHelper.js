({
    fetchCamPlanHelper : function(component, event, helper) {
        component.set('v.mycolumns', [
                {label: 'Campaign Name', fieldName: 'Name', type: 'text'},
                {label: 'Description', fieldName: 'Description__c', type: 'Long Text Area'},
                {label: 'Start Date', fieldName: 'Start Date', type: 'Date'}
            ]);
        var action = component.get("c.fetchCampaignsPlan");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.PlanList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})