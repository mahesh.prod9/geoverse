({
    getAllPrograms : function(component, event, helper) {
        var action = component.get('c.getPrograms');
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                component.set("v.programs",response.getReturnValue()); 
            }
        });
        
        $A.enqueueAction(action);
        
        /* var columns = [  { label: 'Name', fieldName: 'Name', type: 'text' },
                   { label: 'Status', fieldName: 'pmdm__Status__c', type: 'text' },
                   { label: 'Unit of Measurement', fieldName: 'pmdm__UnitOfMeasurement__c', type: 'text' },
                   { label: 'Description', fieldName: 'pmdm__Description__c', type: 'text' },];
                       
         component.set( 'v.mycolumns', columns); */
        
        
        var action =  component.get("c.getServicesInit");
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.data", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    
    handleClick : function(component, event, helper){
        
        var action =  component.get("c.getServices");
        action.setParams({pName :component.get("v.selectedProgram"),startDate : component.get("v.startDate")});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.data", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
    }   
})