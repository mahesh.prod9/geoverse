({
 myAction : function(component, event, helper) 
    {
        
         component.set('v.columns', [
            {label: 'Name', fieldName: 'Name', type: 'text'},
            {label: 'Start Date', fieldName: 'pmdm__StartDate__c', type: 'Date'}
         ]); 
        var programLst = component.get("c.program");
        programLst.setParams
        ({
            contactId: component.get("v.recordId")
            
        });
        
        programLst.setCallback(this, function(data) 
                           {
                               console.log(data.getReturnValue());
                               component.set("v.PrgmEngmtList", data.getReturnValue());
                           });
        console.log('programLst>>'+programLst);
        $A.enqueueAction(programLst);
 }
})