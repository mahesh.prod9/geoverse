({
 createOpportunity : function(component, event, helper) {
        var recordEvent=$A.get("e.force:createRecord");
        recordEvent.setParams({
            "entityApiName": "Opportunity",
            "defaultFieldValues":{
                "AccountId":"0010U000018zbvIQAQ",
                "RecordTypeId":"0120U000001kF9IQAU"
            }
        });
        recordEvent.fire();
 }
})