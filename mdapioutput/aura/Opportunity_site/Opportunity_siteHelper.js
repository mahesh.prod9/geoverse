({
    fetchOppHelper : function(component, event, helper) {
        component.set('v.mycolumns', [
                {label: 'Donation', fieldName: 'Name', type: 'text'},
                {label: 'Amount', fieldName: 'Amount', type: 'Currency'},
            {label: 'Currency', fieldName: 'CurrencyIsoCode', type: 'text'},
                {label: 'Stage Name', fieldName: 'StageName', type: 'Picklist'}
            ]);
        var action = component.get("c.fetchOpportunity");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.oppyList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})