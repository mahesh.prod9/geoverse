({
    handleCreateOpportunity: function(component, event) {
        var saveOpportunityAction = component.get("c.createOpportunity");
        saveOpportunityAction.setParams({
            "opportunity": component.get("v.newOpp")
        });
        
        // Configure the response handler for the action
        saveOpportunityAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.message", "Opportunity created successfully");
                component.set("v.isVisible", false);
            }
            else if (state === "ERROR") {
                console.log('Problem saving Opportunity, response state: ' + state);
            }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
        });
        
        // Send the request to Opportunity the new opportunity
        $A.enqueueAction(saveOpportunityAction);
    },
    scriptsLoaded : function(component, event, helper) {
  },
    fetchOpp : function(component, event, helper) {
         if (window.location.href.indexOf("bot") > -1 || window.location.href.indexOf("YMCA") > -1) {
             component.set('v.isbuttonvisible',true);
         }
        helper.fetchOppHelper(component, event, helper);
    },
    visibleTrue : function(component, event) {
        //component.set("v.isVisible", true);
        if (window.location.href.indexOf("bot") > -1) {
        window.open("https://yicrmdev-sampleeinsteinbot.cs97.force.com/fundingprograms/s/donate","_self");
        }
        else{
            window.open("https://mouritech--yicrmdev.lightning.force.com/lightning/n/Donate","_self");
        }
    },
    visibleFalse : function(component, event) {
        component.set("v.isVisible", false);
    },
    
})