({
    fetchSershHelper : function(component, event, helper) {
        component.set('v.mycolumns', [
                {label: 'Service Schedule Name', fieldName: 'Name', type: 'text'},
                {label: 'Service', fieldName: 'pmdm__Service__c', type: 'Lookup'},
                {label: 'First Session Start', fieldName: 'pmdm__FirstSessionStart__c', type: 'Date/Time'},
                {label: 'First Session End', fieldName: '	pmdm__FirstSessionEnd__c', type: 'Date/Time'}
            ]);
        var action = component.get("c.fetchSersh");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.sershList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})