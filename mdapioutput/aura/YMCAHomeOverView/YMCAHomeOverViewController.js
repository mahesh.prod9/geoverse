({
    renderNotifications : function(component, event, helper) {
        component.set("v.isNotificationOpen",true);
        component.set("v.isMemberInformationOpen",false);
        component.set("v.isFormsOpen",false);
        component.set("v.isPaymentMethodOpen",false);
        component.set("v.isOrderHistoryOpen",false);
        component.set("v.isTransactionHistoryOpen",false);
    },
    renderMemberInformation : function(component, event, helper) {
        component.set("v.isNotificationOpen",false);
        component.set("v.isMemberInformationOpen",true);
        component.set("v.isFormsOpen",false);
        component.set("v.isPaymentMethodOpen",false);
        component.set("v.isOrderHistoryOpen",false);
        component.set("v.isTransactionHistoryOpen",false);	
    },
    renderForms : function(component, event, helper) {
        component.set("v.isNotificationOpen",false);
        component.set("v.isMemberInformationOpen",false);
        component.set("v.isFormsOpen",true);
        component.set("v.isPaymentMethodOpen",false);
        component.set("v.isOrderHistoryOpen",false);
        component.set("v.isTransactionHistoryOpen",false);	
    },
    renderPaymentMethod : function(component, event, helper) {
        component.set("v.isNotificationOpen",false);
        component.set("v.isMemberInformationOpen",false);
        component.set("v.isFormsOpen",false);
        component.set("v.isPaymentMethodOpen",true);;
        component.set("v.isOrderHistoryOpen",false);
        component.set("v.isTransactionHistoryOpen",false);
        
    },
    renderOrderHistory : function(component, event, helper) {
        component.set("v.isNotificationOpen",false);
        component.set("v.isMemberInformationOpen",false);
        component.set("v.isFormsOpen",false);
        component.set("v.isPaymentMethodOpen",false);
        component.set("v.isOrderHistoryOpen",true);
        component.set("v.isTransactionHistoryOpen",false);
        
    },
    renderTransactionHistory : function(component, event, helper) {
        component.set("v.isNotificationOpen",false);
        component.set("v.isMemberInformationOpen",false);
        component.set("v.isFormsOpen",false);
        component.set("v.isPaymentMethodOpen",false);
        component.set("v.isOrderHistoryOpen",false);
        component.set("v.isTransactionHistoryOpen",true);
        
    }
})