({
	subscribe : function(component, event, helper) {
        component.set('v.notifications',[]);
        const empApi = component.find('empApi');
        const channel = '/event/Account_Creation_Event__e';
		// Replay option to get new events
		const replayId = -1;
        //subscribe to an platform event
        empApi.subscribe(channel,replayId,$A.getCallback(eventReceived => {
            // process event(this is called each time we received an event)
            console.log('Received Event', JSON.stringify(eventReceived));
            console.log('Received Event Data::', eventReceived.data);
            console.log('Received event999', eventReceived.data.payload.Account_Name__c);
            var notifications = component.get('v.notifications');
            //console.log(':notifications:111:', notifications);
            notifications.push(eventReceived.data.payload.Account_Name__c);
            component.set('v.notifications', notifications);
            //console.log("notifications after iteration"+JSON.stringify(notifications));
            //console.log(':notifications:222:', notifications);
            component.set('v.evntRecieved', true);
            //console.log(':evntRecieved::', evntRecieved);
        })).then(subscription =>{
            //confirm that we have subscribe to the event channel
            //we haven't received an event yet
            console.log('subscribed to channel',subscription.channel);
            // save subscription to unsubscribe later
            //component.set('v.subscription',subscription);
        });
	}
})