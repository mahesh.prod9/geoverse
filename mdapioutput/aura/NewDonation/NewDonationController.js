({
     fetchOpp : function(component, event, helper) {
        helper.fetchOppHelper(component, event, helper);
          var action = component.get("c.fetchContactName");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.contactname", response.getReturnValue());
                console.log( response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
	 handleCreateOpportunity: function(component, event,helper) {
         component.set('v.isLoading',true);
            var action = component.get("c.createOpportunity");
         console.log(JSON.parse(JSON.stringify(component.get("v.newOpp"))));
        action.setParams({
            "opportunityobj": JSON.stringify(component.get("v.newOpp"))
        });
        //console.log('**'+component.get("v.newOpp"));
        // Configure the response handler for the action
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state', state);
            if(state === "SUCCESS") {
                console.log("Opportunity created successfully");
                         component.set('v.isLoading',false);
                component.set('v.message',true);
                //component.set("v.isVisible", false);
            }
            else if (state === "ERROR") {
                console.log('Problem saving Opportunity, response state: ' + state);
            }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
        });
        
        // Send the request to Opportunity the new opportunity
        $A.enqueueAction(action);
     
    },
    visibleFalse: function(component,event){
        if (window.location.href.indexOf("bot") > -1) {
        window.open("https://yicrmdev-sampleeinsteinbot.cs97.force.com/fundingprograms/s/donations","_self");
        }
        else{
            window.open("https://mouritech--yicrmdev.lightning.force.com/lightning/n/Donations","_self");
        }
    },
    closeModel: function(component,event,helper){
        if (window.location.href.indexOf("bot") > -1) {
        window.open("https://yicrmdev-sampleeinsteinbot.cs97.force.com/fundingprograms/s/donations","_self");
        }
        else{
            window.open("https://mouritech--yicrmdev.lightning.force.com/lightning/n/Donations","_self");
        }
        
    },
    getLookUpValues : function(component, event, helper){
        let fieldName = event.getParam("fieldName");
        let projectId = event.getParam("selectedRecordId");
        
        component.set("v.projectId", projectId);
        console.log('campagin id',component.get("v.projectId"));
    }
})