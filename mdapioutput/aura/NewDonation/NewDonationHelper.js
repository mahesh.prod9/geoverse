({
	 fetchOppHelper : function(component, event, helper) {
        var action = component.get("c.fetchAccountName");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.newOpp.Account_Name__c", response.getReturnValue());
                console.log( response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})