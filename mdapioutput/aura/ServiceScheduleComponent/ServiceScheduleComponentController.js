({
    myAction : function(component, event, helper) 
    {
        component.set('v.columns', [
            {label: 'Schedule Name', fieldName: 'Name', type: 'text'},
            {label: 'Date', fieldName: 'pmdm__Interval__c', type: 'Number'},
            {label: 'Frequency', fieldName: 'pmdm__Frequency__c', type: 'Picklist'}
        ]); 
        var ScheduleList = component.get("c.serviceSchedule");
        ScheduleList.setParams
        ({
            serviceId: component.get("v.ServiceId")
        });
        
        ScheduleList.setCallback(this, function(data) 
                                 {
                                     console.log(data.getReturnValue());
                                     
                                     component.set("v.ServiceScheduleList", data.getReturnValue());
                                 });
        $A.enqueueAction(ScheduleList);
    }
})