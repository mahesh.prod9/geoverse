({
    fetchMembHelper : function(component, event, helper) {
        component.set('v.mycolumns', [
            	{label: 'Account Name', fieldName: 'Account_Name__c', type: 'text'},
                {label: 'Membership Name', fieldName: 'Name', type: 'text'},
            	{label: 'Member Level', fieldName: 'npe01__Member_Level__c', type: 'picklist'},
            	{label: 'Membership Start Date', fieldName: 'npe01__Membership_Start_Date__c', type: 'Date'},
                {label: 'Membership End Date', fieldName: 'npe01__Membership_End_Date__c', type: 'Date'}
            
            ]);
        var action = component.get("c.fetchMemberscntrl");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.membList", response.getReturnValue());
                console.log('Members List',response.getReturnValue()[0].AccountId);
            }
        });
        $A.enqueueAction(action);
    }
})