({
    fetchProgramHelper : function(component, event, helper) {
        component.set('v.mycolumns', [
                {label: 'Service Name', fieldName: 'Name', type: 'text'},
            {label: 'Program Name', fieldName: 'Program_Name__c', type: 'text'},
                {label: 'Service Start Date', fieldName: 'Service_Start_Date__c', type: 'Date'},
                {label: 'Service End Date', fieldName: 'Service_End_Date__c', type: 'Date'}
               /* {label: 'Start Date', fieldName: 'pmdm__StartDate__c', type: 'Date'},
                {label: 'End Date', fieldName: 'pmdm__EndDate__c', type: 'Date'}*/
            ]);
        var action = component.get("c.fetchProgram");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.progList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})