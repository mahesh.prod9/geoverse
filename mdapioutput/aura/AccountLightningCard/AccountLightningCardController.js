({
	toggleSelection: function(component, event, helper) {
		var selectedValue = event.getParam("checked");
        console.log('=========> Selected = ' + selectedValue);
		var accountsSelectedEvent = component.getEvent("AccountsSelectedEvent");
        accountsSelectedEvent.setParams({
            "selected" : selectedValue
        });
        accountsSelectedEvent.fire();
	 }
})