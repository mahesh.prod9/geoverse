({
    handleCreateContact : function(component, event, helper){
             component.set("v.showMandatorymessage",false);
        var labelval = component.get('v.newContact');
        var phone = /[^\d{10}]/;
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(labelval.FirstName == null || labelval.FirstName == undefined || labelval.FirstName == ''){
            component.set("v.showMandatorymessage",true);
            component.set('v.mandatoryErrorMessages', 'Enter First Name');
        }
        if(labelval.LastName == null || labelval.LastName == undefined || labelval.LastName == ''){
             component.set("v.showMandatorymessage",true);
           component.set('v.mandatoryErrorMessages', 'Enter Last Name');
        }
        if(labelval.MobilePhone == null || labelval.MobilePhone == undefined || labelval.MobilePhone == ''){
             component.set("v.showMandatorymessage",true);
           component.set('v.mandatoryErrorMessages', 'Enter Mobile Number');
        }
         if(labelval.Email == null || labelval.Email == undefined || labelval.Email == ''){
             component.set("v.showMandatorymessage",true);
           component.set('v.mandatoryErrorMessages','Enter Email Address');
        }
        if(labelval.MobilePhone.match(phone)){
            alert("enter valid phone number");
            component.set("v.showMandatorymessage",true);
            component.set('v.mandatoryErrorMessages', 'Enter valid phone Number');
        }      
        if(!labelval.Email.match(regExpEmailformat)){
             component.set("v.showMandatorymessage",true);
            component.set('v.mandatoryErrorMessages', 'Enter valid Email Address');
        }
        var action = component.get("c.createContact");
        action.setParams({"contact": component.get("v.newContact")});       
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                console.log( "Contact created successfully");
                component.set("v.isVisible","true");
            }else if(state === 'ERROR'){
                console.log('Problem saving contact, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    cancelclick:function(component,event,helper){
        window.open('https://yicrmdev-sampleeinsteinbot.cs97.force.com/fundingprograms/s/login/','_self');
    },
    Navigate : function(component, event, helper){
        window.open('https://yicrmdev-sampleeinsteinbot.cs97.force.com/fundingprograms/s/','_self');
    },
    showSpinner: function(component, event, helper) { 
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){  
        component.set("v.Spinner", false);
    },
})