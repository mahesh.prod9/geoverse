({
    handleCreateOpportunity: function(component, event) {
        var saveOpportunityAction = component.get("c.createOpportunity");
            saveOpportunityAction.setParams({
                "opportunity": component.get("v.newOpp")
            });
        
        // Configure the response handler for the action
        saveOpportunityAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.message", "Opportunity created successfully");
            }
            else if (state === "ERROR") {
                console.log('Problem saving Opportunity, response state: ' + state);
            }
            else {
                console.log('Unknown problem, response state: ' + state);
            }
        });

        // Send the request to Opportunity the new opportunity
        $A.enqueueAction(saveOpportunityAction);
    },
})