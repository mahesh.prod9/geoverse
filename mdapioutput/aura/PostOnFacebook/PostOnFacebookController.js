({   
    doInit : function (component, event, helper){
        
        var cmpId = component.get("v.recordId");
        var action =  component.get("c.post");
        action.setParams({CampId :cmpId});
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.Description",response.getReturnValue());

            }else {
                
                component.set("v.Description",response.getReturnValue());
            }
         
        });
        $A.enqueueAction(action);
    }
})