({
 myAction : function(component, event, helper) 
    {
         component.set('v.columns', [
            {label: 'Service name', fieldName: 'Name', type: 'text'},
            {label: 'Start Date', fieldName: 'Start_Date__c', type: 'Date'},
            {label: 'Status', fieldName: 'pmdm__Status__c', type: 'Picklist'}
                    ]); 
        var serviceLst = component.get("c.service");
        serviceLst.setParams
        ({
            recordId: component.get("v.programId")
            
        });
        
        
        serviceLst.setCallback(this, function(data) 
                           {
                               console.log(data.getReturnValue());
                               component.set("v.ServiceList", data.getReturnValue());
                           });
        console.log('serviceLst>>'+serviceLst);
        $A.enqueueAction(serviceLst);
 }
})