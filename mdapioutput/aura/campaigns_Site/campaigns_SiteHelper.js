({
    fetchCamHelper : function(component, event, helper) {
        component.set('v.mycolumns', [
                {label: 'Campaign Name', fieldName: 'Name', type: 'text'},
                {label: 'Status', fieldName: 'Status', type: 'Picklist'},
                {label: 'Type', fieldName: 'Type', type: 'Picklist'}
            ]);
        var action = component.get("c.fetchCampaigns");
        action.setParams({
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.camList", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})