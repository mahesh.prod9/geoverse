import BaseChatMessage from 'lightningsnapin/baseChatMessage';
import { LightningElement, track } from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import BaseChatHeader from 'lightningsnapin/baseChatHeader';
 
const CHAT_CONTENT_CLASS = 'chat-content';
const AGENT_USER_TYPE = 'agent';
const CHASITOR_USER_TYPE = 'chasitor';
const SUPPORTED_USER_TYPES = [AGENT_USER_TYPE, CHASITOR_USER_TYPE];
 
export default class lwcSampleProgram extends BaseChatMessage
{
    @track strMessage = '';
    //Add a var to track visibility for the component
    @track isBaseTextVisible = false;
    @track bru = false;
    @track datepicker = false;
    
    connectedCallback() 
    {
        this.strMessage = this.messageContent.value;
        if (this.isSupportedUserType(this.userType)) 
        {
            //if using a lwc, remove any emojis that may have been inserted by the bot (ie :D or :p )
            /*if (this.userType == 'agent' && this.messageContent.value.startsWith('lwc'))
            {
                this.strMessage = this.strMessage.replace(/😀/g, ':D').replace(/😛/g, ':p');
            }*/
            if (this.userType == 'agent' && this.messageContent.value.startsWith('lwc:bru'))
            {
               // this.isBaseTextVisible = false;
                this.bru = true;
            }
            if (this.userType == 'agent' && this.messageContent.value.startsWith('lwc:datepicker'))
            {
               // this.isBaseTextVisible = false;
                this.datepicker = true;
            }
           /* else (!this.messageContent.value.startsWith('lwc:hide'))
            {
                this.isBaseTextVisible = true;
                this.messageStyle = `${CHAT_CONTENT_CLASS} ${this.userType}`;
            }*/
            else if (!this.messageContent.value.startsWith('lwc:hide'))
            {
                this.isBaseTextVisible = true;
                this.messageStyle = `${CHAT_CONTENT_CLASS} ${this.userType}`;
            }
        }
        else
        {
            throw new Error('Unsupported user type passed in: ${this.userType}');
        }
    }
            isSupportedUserType(userType) 
        {
        return SUPPORTED_USER_TYPES.some((supportedUserType) => supportedUserType === userType);
        }
        handlePostMessage(event) 
        {
        const dateValue = event.detail;
        console.log('Handling Event with value: ' + dateValue);
        window.postMessage(
            {
                message: dateValue,
                type: "chasitor.sendMessage"
            },
            window.parent.location.href
        );
            
        
         }
}