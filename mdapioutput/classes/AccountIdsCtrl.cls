public class AccountIdsCtrl {
	@AuraEnabled 
    public static List<String> getAccountIds(){
        List<String> strList = new List<String>();
        List<Account> accList = [SELECT Id, Name FROM Account LIMIT 10];
        for(Account acc:accList){
            strList.add(acc.Id);
        }
        return strList;
    }
}