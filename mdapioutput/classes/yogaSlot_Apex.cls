public with sharing class yogaSlot_Apex {
    public class YogaOutput{
        @InvocableVariable(required=true)
        public string YogaTimeSlots = '';
    }
    @InvocableMethod(label=' Get Time Slot')
    public static list<list<pmdm__ServiceSession__c>>  GetTimeSlots() {
         DateTime currentTime = datetime.now();
          DateTime  currentTime1= currentTime.addHours(12);
        list<list<pmdm__ServiceSession__c>> yoagaslots = new list<list<pmdm__ServiceSession__c>>();
        list<pmdm__ServiceSession__c>slotsname =[select id,CreatedDate,Name,pmdm__ServiceSchedule__r.Name,pmdm__SessionStart__c from pmdm__ServiceSession__c where pmdm__ServiceSchedule__r.Name LIKE 'Session2'  AND pmdm__SessionStart__c >=: currentTime];
        yoagaslots.add(slotsname);
        return  yoagaslots;
        /*   for( pmdm__ServiceSession__c Timesl : slotsname){
          a.YogaTimeSlots = a.YogaTimeSlots+' , '+Timesl.pmdm__SessionStart__c;
            if(!(a.YogaTimeSlots == Null) || !(a.YogaTimeSlots == '') )
               system.debug('slottime'+ a.YogaTimeSlots);
             }     
            system.debug(yoagaslots);*/
    }
}