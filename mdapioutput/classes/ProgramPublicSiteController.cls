public Without Sharing class ProgramPublicSiteController {
    
    @AuraEnabled
    Public static list<ProgramWrapper> InitMethod(){
        
        set<Id> PmIdset = new set<Id>();
        list<pmdm__Service__c> lst_Service = [SELECT pmdm__Program__c FROM pmdm__Service__c where pmdm__Program__c != null];
        for(pmdm__Service__c P : lst_Service){
          PmIdset.add(p.pmdm__Program__c) ; 
        } 
        
        list<ProgramWrapper> wraplist = new list<ProgramWrapper>();      
        list<pmdm__Program__c> lst_Program = [Select id,name,(Select Id,Name,Start_Date__c, End_Date__c, pmdm__Status__c from pmdm__Services__r)from pmdm__Program__c
                                             where id in :PmIdset];
        
        for(pmdm__Program__c p : lst_Program){
            wraplist.add(new ProgramWrapper(p,p.pmdm__Services__r));
        }
     return wraplist;
    }
    
     public class ProgramWrapper{
         
     @AuraEnabled Public  pmdm__Program__c P;  
      @AuraEnabled Public list<pmdm__Service__c> serviceList;
        
        public ProgramWrapper(pmdm__Program__c P,list<pmdm__Service__c> serviceList ){
         This.P = P;
         This.serviceList = serviceList;    
        }      
    }

}