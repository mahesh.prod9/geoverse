public without sharing class opportunity_Site_Class{
    
    @AuraEnabled
    public static boolean createOpportunity(String opportunityobj){
        system.debug('***Opp' +opportunityobj);
        Opportunity p = (Opportunity)JSON.deserialize(opportunityobj, Opportunity.class);
 system.debug('***Opp1' +p);
        Id donRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Donation').getRecordTypeId();
         User userrec = [Select Id,profile.Name, ContactId,contact.AccountId,Contact.Account.Name from User where Id =: UserInfo.getUserId()];
        string profilename = userrec.profile.Name;
        p.RecordTypeId = donRecordTypeId;
        p.StageName = 'Pledged';
        p.AccountId = '0010U000018zbvIQAQ';
        if(profilename.contains('Grantseeker')){
         p.AccountId = userrec.contact.AccountId;
            p.Name = userrec.contact.Account.Name+' Donation';
        }
        System.debug('opportunity' + p);
        try {
            insert p;
        }
        Catch(Exception e) {
            System.debug('Insert is failed' + e.getMessage() + e.getLineNumber());
        }
          
        return true;
    }
    
    @AuraEnabled
    public static List <Opportunity> fetchOpportunity() {
       Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Donation').getRecordTypeId();
         User userrec = [Select Id,profile.Name, ContactId,contact.AccountId from User where Id =: UserInfo.getUserId()];
        system.debug('recor type'+devRecordTypeId);
        /*List<Opportunity> oppList = [SELECT Id, Name,Amount, StageName from Opportunity where Ownerid=:userinfo.getuserId() 
                                    And recordtypeId =: devRecordTypeId];*/
         string profilename = userrec.profile.Name;
        List<Opportunity> oppList = [SELECT Id, Name,Amount,CurrencyIsoCode,Account_Name__c, StageName from Opportunity where 
                                    recordtypeId =: devRecordTypeId and OwnerId = : UserInfo.getUserId() order by createddate desc limit 10];
          if(!profilename.contains('Grantseeker')){
              oppList = [SELECT Id, Name,Amount,CurrencyIsoCode, StageName from Opportunity where 
                                    recordtypeId =: devRecordTypeId  order by createddate desc limit 10];
          }
        return oppList;
    }
    
    @AuraEnabled
    public static string fetchAccountName(){
         User userrec = [Select Id,profile.Name, ContactId,contact.Account.Name from User where Id =: UserInfo.getUserId()];
        return userrec.contact.Account.Name;
    }
    
    @AuraEnabled
    public static string fetchContactName(){
         User userrec = [Select Id,profile.Name, ContactId,contact.name,contact.Account.Name from User where Id =: UserInfo.getUserId()];
        return userrec.contact.Name;
    }
}