public without sharing class oppy_Membership_Class{
    @AuraEnabled
    public static List <Opportunity> fetchOpportunity() {
       Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Membership').getRecordTypeId();
        system.debug('recor type'+devRecordTypeId);
        /*List<Opportunity> oppList = [SELECT Id, Name,Amount, StageName from Opportunity where Ownerid=:userinfo.getuserId() 
                                    And recordtypeId =: devRecordTypeId];*/
        List<Opportunity> oppList = [SELECT Id, Name,Amount, StageName from Opportunity where 
                                    recordtypeId =: devRecordTypeId];
        System.debug('oppList' + oppList);
        return oppList;
    }
}