public without sharing class ProgramServiceSearchController {
    
    @AuraEnabled
    Public static  list<string> getPrograms(){
        list<string> lst_PrName = new list<string>();
        list<pmdm__Program__c> lst_Program = [select id, name from pmdm__Program__c];
        for(pmdm__Program__c p : lst_Program){
            lst_PrName.add(p.Name); 
        }
        return lst_PrName;
    }
    
    @AuraEnabled
    public static list<pmdm__Service__c> getServicesInit(){
        list<pmdm__Service__c> lst_pmdm_Service = [select id,Name,	pmdm__Status__c,pmdm__Program__r.Name,Program_Name__c, pmdm__UnitOfMeasurement__c,pmdm__Description__c,Start_Date__c, End_Date__c,Service_Image__c
                                                   from pmdm__Service__c limit 8];  
        return lst_pmdm_Service;
    }
    @AuraEnabled
    public static list<pmdm__Service__c> getServices(string pName, string startDate){
        list<pmdm__Service__c> lst_pmdm_Service = new list<pmdm__Service__c>();
        
        string query = 'select id,Name,pmdm__Status__c,pmdm__Program__r.Name, pmdm__UnitOfMeasurement__c,pmdm__Description__c, Start_Date__c, End_Date__c,Service_Image__c,Program_Name__c from pmdm__Service__c';
        String queryCondition = '';
        //Date sDate = Date.valueOf(startDate); 
        if(string.isNotBlank(pName) && pName != '--None--') {
            queryCondition = 'pmdm__Program__r.Name = ' + '\'' + pName + '\'';
        }
        if(string.isNotBlank(startDate)) {
            String result = String.isBlank(queryCondition) ? 'pmdm__Program__r.pmdm__StartDate__c = ' : 'AND pmdm__Program__r.pmdm__StartDate__c = ';
            queryCondition = queryCondition + result + startDate;
        } 
        
        if(String.isNotBlank(queryCondition)) {
            query = query + ' Where ' + queryCondition;
            System.debug('query' + query);
        }
          
        System.debug('query' + query);
        lst_pmdm_Service = Database.query(query);       
        system.debug(lst_pmdm_Service);
              
        return  lst_pmdm_Service;              
    }
    
}