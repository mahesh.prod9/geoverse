public without sharing class MemberInformationController {
    
    @AuraEnabled
    public static list<MembersWrapper> getMembers(){
       
        Id  userId = UserInfo.getUserId();
        user u = [select id, lastname,email from user where id = :userId];
        
        list<npe4__Relationship__c> Members = [select id,Name,	npe4__Status__c,npe4__Type__c,npe4__Description__c,npsp__Related_Opportunity_Contact_Role__c,
                                               npe4__RelatedContact__r.Name,npe4__RelatedContact__r.Address__c,npe4__RelatedContact__r.Email, npe4__Contact__r.Email
                                               from npe4__Relationship__c where  npe4__Contact__r.Email = :u.Email ];
        list<MembersWrapper> lst_members = new  list<MembersWrapper>();
        
        for(npe4__Relationship__c obj : Members){
          lst_members.add(new MembersWrapper(obj.npe4__RelatedContact__r.Name,obj.npe4__Type__c,obj.npe4__Status__c,obj.npe4__Description__c,obj.npe4__RelatedContact__r.Address__c) ) ; 
        }
        return lst_members ;
    }
    
    public class MembersWrapper{
       @AuraEnabled public String RelatedContact;
       @AuraEnabled public string type;
       @AuraEnabled public string status;
       @AuraEnabled public String Description;
       @AuraEnabled public string Address;
        
        public MembersWrapper(string RelatedContact, string type, string status,string Description,string Address  ){
            this.RelatedContact = RelatedContact;
            this.type = type; 
            this.status = status;
            this.Description = Description;
            this.address = address;
            
        }
    }

}