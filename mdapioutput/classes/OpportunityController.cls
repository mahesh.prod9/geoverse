public with sharing class OpportunityController {
    
    @AuraEnabled
    public static Opportunity createOpportunity(Opportunity opportunity){
        Id donRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Donation').getRecordTypeId();
        opportunity.RecordTypeId = donRecordTypeId;
        opportunity.AccountId = '0010U000018zbvIQAQ';
        System.debug('opportunity' + opportunity);
        try {
            insert opportunity;
        }
        Catch(Exception e) {
            System.debug('Insert is failed' + e.getMessage() + e.getLineNumber());
        }
          
        return opportunity;
    }
}