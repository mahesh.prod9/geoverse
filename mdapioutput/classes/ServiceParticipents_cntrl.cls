public class ServiceParticipents_cntrl {
    @AuraEnabled
    Public static List<pmdm__ServiceParticipant__c> servicePartcpnt(String scheduleId){
        system.debug('scheduleId>>'+scheduleId);
        List<pmdm__ServiceParticipant__c> sp=[Select Id, pmdm__ServiceSchedule__c ,pmdm__ServiceSchedule__r.Name, Name from pmdm__ServiceParticipant__c where pmdm__ServiceSchedule__c=:scheduleId];
        
        system.debug('sp>>'+sp);
        return sp;
        
    } 
    @AuraEnabled
    Public static List<pmdm__ServiceSchedule__c > serviceSchedule(String serviceId){
        system.debug('serviceId>>'+serviceId);
        List<pmdm__ServiceSchedule__c> ss=[Select Id, pmdm__DaysOfWeek__c,pmdm__ServiceScheduleEnds__c,pmdm__ParticipantCapacity__c,pmdm__PrimaryServiceProvider__c, pmdm__Frequency__c, pmdm__Service__c, Name from pmdm__ServiceSchedule__c where pmdm__Service__c=:serviceId];
        
        system.debug('ss>>'+ss);
        return ss;
        
    } 
    @AuraEnabled
    Public static List<pmdm__Service__c > service(String recordId){
        system.debug('recordId>>'+recordId);
        List<pmdm__Service__c> serviceLst=[Select Id,Start_Date__c,pmdm__Program__c,Program_Name__c,pmdm__Status__c, Name from pmdm__Service__c where pmdm__Program__c=:recordId];
        
        system.debug('serviceLst>>'+serviceLst);
        return serviceLst;
        
    }
    
    @AuraEnabled
    Public static list<pmdm__ProgramEngagement__c> program(String contactId){
        list<pmdm__ProgramEngagement__c>  programLst=[SELECT Id, Name, pmdm__Account__c, pmdm__Contact__r.Name, pmdm__Program__r.Name FROM pmdm__ProgramEngagement__c where pmdm__Contact__c =:contactId];
        
        system.debug('programLst>>'+programLst);
        return programLst;
        
    }
    
    
}