public without sharing class loginUserProfileCtrl {
    @AuraEnabled 
    public static user fetchUserDetail(){
        return [Select id,Name,SmallPhotoUrl, FullPhotoUrl
                From User
                Where Id =: Userinfo.getUserId()];
    }
    
    @AuraEnabled
    public static Contact getContact(){
        String userName = UserInfo.getUserName();
        User activeUser = [Select Email From User where Username = : userName limit 1];
        String userEmail = activeUser.Email;
        Contact con = [SELECT Id, LastName, FirstName, Phone, Email, Address__c FROM Contact where Email =: userEmail limit 1];
        return con;   
    }
    
}