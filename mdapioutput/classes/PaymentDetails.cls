public without sharing class PaymentDetails {
@AuraEnabled
    public static List<npe01__OppPayment__c> getDetails(){
        return [select id,npe01__Payment_Amount__c,Name,npe01__Payment_Method__c,npe01__Payment_Date__c from npe01__OppPayment__c];
    }
}