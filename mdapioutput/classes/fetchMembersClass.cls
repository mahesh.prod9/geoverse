public without sharing class fetchMembersClass {
    @AuraEnabled
    public static List <Opportunity> fetchMemberscntrl() {
       Id devRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Membership').getRecordTypeId();
        system.debug('record type'+devRecordTypeId);
        List<Opportunity> oppList = [SELECT Id, Name,Amount,npe01__Member_Level__c,npe01__Membership_Start_Date__c,npe01__Membership_End_Date__c, Account_Name__c  from Opportunity where 
                                    recordtypeId =: devRecordTypeId];
        System.debug('oppList' + oppList);
        return oppList;
    }

}