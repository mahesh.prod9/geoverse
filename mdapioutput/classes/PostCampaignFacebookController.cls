public class PostCampaignFacebookController {
    
    public static string client_id = '738761066834582';
    public static string client_secret = '49c3714e1a25c5c1240b9d60be6cc28b';
    public static string exchange_token = 'EAAKf5jb2apYBAKnmZCzy0rjtsKqZCZCsZCMT5y4fYybxKRYZBuTfLzDyvfALRRA969sqIb5A0hMvEcvZCUzMGkg9uAXEC7dd0WgcSZBaLwMdTopwAhZBi8ByShdaxZCLdHdfY0qiqr46gVYCs13OWQzB4WDPdZAxMwmflYIdTlZC4i8ZBCVsKKAqgHhG';   
    public static campaign cmp;
   
   
    
   /* 
    public PostCampaignFacebookController(ApexPages.StandardController stdController) {
        this.cmp = (campaign)stdController.getRecord();
        string cId = apexpages.currentpage().getparameters().get('id');
        cmp1 = [select id,description,description__c from campaign where id = :cId];
        
    }*/
    
    
    
    public static string getAccessToken(){
        
        http obj_http = new http();
        httpRequest request = new httpRequest();
        request.setMethod('GET');
        // string Endpoint = 'https://graph.facebook.com/v10.0/oauth/access_token';
        string Endpoint = 'https://graph.facebook.com/v10.0/oauth/access_token';
        Endpoint += '?grant_type=fb_exchange_token';
        Endpoint += '&client_id='+client_id;
        Endpoint += '&client_secret=' + client_secret;
        Endpoint += '&fb_exchange_token=' + exchange_token;
        system.debug(Endpoint);
        request.setEndpoint(Endpoint);
        httpresponse obj_response = obj_http.send(request);
        system.debug(obj_response.getStatus());
        system.debug(obj_response.getBody());
        Map<string,object> resultMap = (map<string,object>)json.deserializeUntyped(obj_response.getBody());
        string accessKey = string.valueOf(resultMap.get('access_token'));   
        system.debug(accessKey);
        return accessKey;
    }
    
    @AuraEnabled
    public Static string post(String CampId){
        string accessKey = getAccessToken();
        cmp = [select id,description,description__c from campaign where id = :CampId];
        
        http obj_http = new http();
        httpRequest request = new httpRequest();
        string endpoint = 'https://graph.facebook.com/103517845163305/feed';
        request.setEndpoint(endpoint);
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('POST');
        postWrapper obj = new postWrapper(cmp.Description  ,accessKey,'bearer');
        string body =  json.serialize(obj);
        request.setBody(body);
        system.debug(body);
        httpResponse response = obj_http.send(request);
        system.debug(response.getStatus());
        system.debug(response.getBody());
        string result = 'Process failed, Campaign is already posted';
        if(response.getStatusCode() == 200){
            Map<string,object> resultMap = (map<string,object>)json.deserializeUntyped(response.getBody());
            string PostId = string.valueOf(resultMap.get('id'));
            system.debug('Campaign Posted successfully with Id: '+ PostId);
            cmp.description__c = 'Campaign Posted successfully with Id: '+ PostId;
            try {
                update cmp;
                result = 'Campaign Posted successfully with Id ' + PostId;
            } Catch(Exception e) {
                System.debug('Updation campaign is failed in ' + e.getLineNumber() + e.getMessage());
            }
            
        }
       return result; 
        
    }
    
    public class postWrapper{
        public string message;
        public string access_token;
        public string token_type;
        
        public postWrapper(string message, string access_token, string token_type ){
            This.message = message;
            This.access_token = access_token;
            This.token_type = token_type;
        }
        
    }
    
}