public without sharing class service_ScheduleClass{
    @AuraEnabled
    public static List <pmdm__ServiceSchedule__c> fetchSersh() {
        User userrec = [Select Id, ContactId from User where Id =: UserInfo.getUserId()];
        List<pmdm__ServiceSchedule__c> sershList = [SELECT Id, Name,pmdm__Service__c,
                                pmdm__FirstSessionStart__c,pmdm__FirstSessionEnd__c,OwnerId, pmdm__OtherServiceProvider__c from pmdm__ServiceSchedule__c 
                                                    where pmdm__OtherServiceProvider__c =: userrec.ContactId];
        return sershList;
    }
}