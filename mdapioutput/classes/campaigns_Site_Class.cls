public without sharing class campaigns_Site_Class{
    @AuraEnabled
    public static List <Campaign> fetchCampaigns() {
        List<Campaign> camList = [SELECT Id, Name,Status,Type from Campaign];
        return camList;
    }
}