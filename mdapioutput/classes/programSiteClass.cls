public without sharing class programSiteClass{
    @AuraEnabled
    public static List <pmdm__ServiceParticipant__c> fetchProgram() {
         User userrec = [Select Id, ContactId from User where Id =: UserInfo.getUserId()];
        List<pmdm__ServiceParticipant__c> ProgramList = [SELECT Id, Name,Service_Name__c,Program_Name__c,pmdm__Contact__c,Service_Start_Date__c,Service_End_Date__c,OwnerId from pmdm__ServiceParticipant__c where pmdm__Contact__c =: userrec.ContactId];
        return ProgramList;
    }
}