public with sharing class NextYogaSessions_apex {
   /*  public class YogaOutput{
    @InvocableVariable(required=true)
     public string YogaTimeSlots = '';
     
  }*/
   /* public class YogaInput{
    @InvocableVariable(required=true)
     public String timeVar = '';
  }*/
    @InvocableMethod(label=' Get Time Slots')
  public static list<list<pmdm__ServiceSession__c>> GetNextTimeSlots() {
      /*string sersch='';
      for(YogaInput yip : timevar){
      if( yip.timeVar == '5:00AM'){
          sersch = 'Session1';
      }
          if( yip.timeVar == '6:00AM'){
          sersch = 'Session2';
      }
          if( yip.timeVar == '7:00AM'){
          sersch = 'Session3';
      }
          
      }*/
       DateTime currentTime = datetime.now();
      System.debug('currentTime: '+currentTime);
       DateTime nxtDay = currentTime.AddDays(7);
      System.debug('nxtDay: '+nxtDay);
       list<list<pmdm__ServiceSession__c>> yoagaslots = new list<list<pmdm__ServiceSession__c>>();
      list<pmdm__ServiceSession__c> slotsname =[select id,CreatedDate,SessionName__c,Name,pmdm__ServiceSchedule__r.Name,pmdm__SessionStart__c from pmdm__ServiceSession__c where pmdm__ServiceSchedule__r.Name = 'Session1' AND pmdm__SessionStart__c >: currentTime AND pmdm__SessionStart__c <: nxtDay];   
     System.debug('Slotsname: '+ slotsname);
      yoagaslots.add(slotsname);
      System.debug('yoagaslots: '+ yoagaslots);
        return  yoagaslots;
      }     
   
}