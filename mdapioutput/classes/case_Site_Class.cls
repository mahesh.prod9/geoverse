public without sharing class case_Site_Class{
    @AuraEnabled
    public static List <Case> fetchCase() {
        List<case> caseList = [SELECT Id, CaseNumber, Type, Status FROM Case where status = 'New'];
        system.debug('case list'+caseList);
        return caseList;
    }
}