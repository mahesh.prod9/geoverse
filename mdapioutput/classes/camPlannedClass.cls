public without sharing class camPlannedClass{
    @AuraEnabled
    public static List <Campaign> fetchCampaignsPlan() {
        List<Campaign> camPlanList = [Select id,name,Description__c,StartDate from Campaign where status = 'Planned'];
        return camPlanList;
    }
}