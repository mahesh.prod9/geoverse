public class ServiceSchedule_cntrl {
@AuraEnabled
    Public static List<pmdm__ServiceSchedule__c > servicePartcpnt(String scheduleId){
        List<pmdm__ServiceSchedule__c> sp=[Select Id, pmdm__DaysOfWeek__c, pmdm__Frequency__c, pmdm__Service__c, Name from pmdm__ServiceSchedule__c where pmdm__Service__c=:scheduleId];
       
        system.debug('sp>>'+sp);
        return sp;
        
    } 
}