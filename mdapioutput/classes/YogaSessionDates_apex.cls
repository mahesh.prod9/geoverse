public class YogaSessionDates_apex {
   public class YogaInput{
    @InvocableVariable(required=true)
     public String timeVar = '';
  }
    @InvocableMethod(label=' Get Week Time Slots')
  public static list<list<pmdm__ServiceSession__c>> GetNextTimeSlots() {
     
       DateTime currentTime = datetime.now();
      // DateTime nxtDay = currentTime.addHours(24);
       list<list<pmdm__ServiceSession__c>> yoagaslots = new list<list<pmdm__ServiceSession__c>>();
      list<pmdm__ServiceSession__c> slotsname =[select id,CreatedDate,SessionName__c,Name,pmdm__ServiceSchedule__r.Name,pmdm__SessionStart__c from pmdm__ServiceSession__c where pmdm__ServiceSchedule__r.Name = 'session1' and pmdm__SessionStart__c >: currentTime LIMIT 6];
          yoagaslots.add(slotsname);
        return  yoagaslots;
      }     
   
}