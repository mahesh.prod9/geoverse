<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_for_Task_Due_in_1_Day_Geoverse</fullName>
        <description>Email Alert for Task Due in 1 Day Geoverse</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Task_Due_in_1_Day_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Past_Due_Task_Reminder_Geoverse</fullName>
        <description>Past Due Task Reminder - Geoverse</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Task_Past_Due_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Past_Duw_Task_21_Days_Reminder_Email_Geoverse</fullName>
        <description>Past Duw Task 21 Days Reminder Email Geoverse</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Task_Past_Due_Reminder_1</template>
    </alerts>
    <outboundMessages>
        <fullName>Case_Notes_to_SVCNOW</fullName>
        <apiVersion>50.0</apiVersion>
        <description>Sends Case Notes/Comments to Service Now via Integration Hub</description>
        <endpointUrl>https://atni.service-now.com/api/sn_sforce_v2_spoke/salesforce_webhook_callbacks/wh_entry</endpointUrl>
        <fields>Description</fields>
        <fields>Id</fields>
        <fields>Status</fields>
        <fields>Type</fields>
        <fields>WhatId</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>snow@atni.com</integrationUser>
        <name>Case Notes to SVCNOW</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Send Case Notes %28tasks%29 to ServiceNow</fullName>
        <actions>
            <name>Case_Notes_to_SVCNOW</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>GTT: Agent Note</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Type</field>
            <operation>equals</operation>
            <value>Note</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.Description</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task Overdue - Geoverse</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Task.IsReminderSet</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Choice Task - Geoverse</value>
        </criteriaItems>
        <description>This is part of Geoverse</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Past_Duw_Task_21_Days_Reminder_Email_Geoverse</name>
                <type>Alert</type>
            </actions>
            <timeLength>21</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Past_Due_Task_Reminder_Geoverse</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Task Reminder - Geoverse</fullName>
        <actions>
            <name>Email_Alert_for_Task_Due_in_1_Day_Geoverse</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.IsReminderSet</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Choice Task - Geoverse</value>
        </criteriaItems>
        <description>Task is ! Day out from being due. This is part of Geoverse</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
