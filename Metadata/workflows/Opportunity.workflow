<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_notice_to_user</fullName>
        <description>Approval notice to user</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deal_submission_ZApproved</template>
    </alerts>
    <alerts>
        <fullName>Billing_Start_Date_Alert</fullName>
        <ccEmails>anilkothapally123@gmail.com</ccEmails>
        <description>Billing Start Date Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>maheswaram.in@mouritech.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Notification_to_Accounting_from_Deal</template>
    </alerts>
    <alerts>
        <fullName>Business_Leader_Review_Alert</fullName>
        <description>Business Leader Review Alert Westnet</description>
        <protected>false</protected>
        <recipients>
            <recipient>maheswaram.in@mouritech.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Business_Leader_Review_Email</template>
    </alerts>
    <alerts>
        <fullName>Business_Leader_Review_Alert_NTUAW</fullName>
        <ccEmails>vtsosie@atni.com</ccEmails>
        <description>Business Leader Review Alert NTUAW</description>
        <protected>false</protected>
        <recipients>
            <recipient>maheswaram.in@mouritech.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Business_Leader_Review_Email</template>
    </alerts>
    <alerts>
        <fullName>Cat_2_After_4_days</fullName>
        <description>Cat 2 After 4 days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Cat_2_Deal_reminder</template>
    </alerts>
    <alerts>
        <fullName>Contract_Expiring_within_45_Days_Email_Alert</fullName>
        <ccEmails>anilkothapally123@gmail.com</ccEmails>
        <description>Contract Expiring within 45 Days Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>maheswaram.in@mouritech.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Contract_Expiration_Email</template>
    </alerts>
    <alerts>
        <fullName>Deal_Stage_Changed_to_Closed_Email_Sent_to_Eng</fullName>
        <description>Deal Stage Changed to Closed Email Sent to Eng</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>maheswaram.in@mouritech.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Closed_Stage_Eng_Notification</template>
    </alerts>
    <alerts>
        <fullName>Deal_Stage_Changed_to_Req_for_Quote_Sent_to_Eng</fullName>
        <ccEmails>JWilliamson@atni.com</ccEmails>
        <ccEmails>SDevlin@atni.com</ccEmails>
        <ccEmails>ZBurke@atni.com</ccEmails>
        <ccEmails>CKennedy@atni.com</ccEmails>
        <description>Deal Stage Changed to Req for Quote Sent to Eng</description>
        <protected>false</protected>
        <recipients>
            <recipient>maheswaram.in@mouritech.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Req_for_Quote_Sent_to_Eng_Notification</template>
    </alerts>
    <alerts>
        <fullName>Deal_Stage_Changed_to_Req_for_Quote_Sent_to_Eng_NTUAW</fullName>
        <description>Deal Stage Changed to Req for Quote Sent to Eng - NTUAW</description>
        <protected>false</protected>
        <recipients>
            <recipient>maheswaram.in@mouritech.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Req_for_Quote_Sent_to_Eng_Notification</template>
    </alerts>
    <alerts>
        <fullName>Deal_recall_notification</fullName>
        <description>Deal_recall_notification2</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>hwong@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shemerskine@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Expiration_Email</template>
    </alerts>
    <alerts>
        <fullName>Deal_rejected</fullName>
        <description>Deal rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ContactFollowUpSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Deal_submission_notification</fullName>
        <description>Deal_submission_notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>hwong@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>shemerskine@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deal_submission_template</template>
    </alerts>
    <alerts>
        <fullName>Deal_submission_receipt_notice</fullName>
        <description>Deal_submission_receipt_notice</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deal_submission_receipt</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Hilton</fullName>
        <description>Email to Hilton</description>
        <protected>false</protected>
        <recipients>
            <recipient>hwong@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deal_submission_template</template>
    </alerts>
    <alerts>
        <fullName>Final_approval_notification</fullName>
        <description>Final_approval_notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deal_approval_template</template>
    </alerts>
    <alerts>
        <fullName>Final_rejection_notification</fullName>
        <description>Final_rejection_notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Expiration_Email</template>
    </alerts>
    <alerts>
        <fullName>Geoverse_Contract_Expiring_in_30_Days</fullName>
        <description>Geoverse Contract Expiring in 30 Days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Expiring_in_30_Days_Geoverse</template>
    </alerts>
    <alerts>
        <fullName>Handoff_package_alert</fullName>
        <ccEmails>CKennedy@atni.com,JWilliamson@atni.com;SDevlin@atni.com</ccEmails>
        <description>Handoff package alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>maheswaram.in@mouritech.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Implementation_Handoff_Package_Sent_alert</template>
    </alerts>
    <alerts>
        <fullName>Hilton_s_approval_email</fullName>
        <description>Hilton&apos;s_approval_email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deal_submission_approval</template>
    </alerts>
    <alerts>
        <fullName>Hilton_s_rejection_email</fullName>
        <description>Hilton&apos;s_rejection_email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ContactFollowUpSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Initial_Submission</fullName>
        <description>Initial Submission</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>hwong@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Expiration_Email</template>
    </alerts>
    <alerts>
        <fullName>Notice_to_approver</fullName>
        <description>Notice to approver</description>
        <protected>false</protected>
        <recipients>
            <recipient>hwong@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deal_submission_approval</template>
    </alerts>
    <alerts>
        <fullName>Notice_to_user</fullName>
        <description>Notice to user</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Deals_Submission_ZNM_to_User</template>
    </alerts>
    <alerts>
        <fullName>Notify_team_of_pending_Contract_Expiration</fullName>
        <description>Notify team of pending Contract Expiration</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>hwong@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rcruz@atni.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vishwardin@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Opportunity_Templates/Contract_Expired_Notice</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_No_Updates_30_days</fullName>
        <description>Opportunity No Updates 30 days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>deborah@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_No_Updates</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Stage_Approval</fullName>
        <description>Opportunity Stage Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>deborah@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approval_Opportunity_Email</template>
    </alerts>
    <alerts>
        <fullName>Past_due_opportunities_email</fullName>
        <description>Past due opportunities email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Expiration_Email</template>
    </alerts>
    <alerts>
        <fullName>Priority_Email_report</fullName>
        <description>Priority Email report</description>
        <protected>false</protected>
        <recipients>
            <recipient>maheswaram.in@mouritech.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Priority_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Revenue_Stage_Approval</fullName>
        <description>Revenue Stage Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>deborah@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Approval_Opportunity_Email</template>
    </alerts>
    <alerts>
        <fullName>Step1_Action</fullName>
        <description>Step1_Action</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>hwong@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Expiration_Email</template>
    </alerts>
    <alerts>
        <fullName>Step2</fullName>
        <description>Step2</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>hwong@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Expiration_Email</template>
    </alerts>
    <alerts>
        <fullName>Step3</fullName>
        <description>Step3</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>hwong@gtt.co.gy</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Contract_Expiration_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_field_update</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Pending</literalValue>
        <name>Approval_field_update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_field_updates</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval_field_updates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deal_Approval_Action</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Approved</literalValue>
        <name>Deal_Approval_Action</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deal_Approvals_Process</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Pending</literalValue>
        <name>Deal_Approvals_Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deal_Field_Update</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Approved</literalValue>
        <name>Deal_Submission_Field_Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deal_Recall</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Recall</literalValue>
        <name>Deal_Recall</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deal_Rejection</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Rejected</literalValue>
        <name>Deal_Rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Field_rejection</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Rejected</literalValue>
        <name>Field_rejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opp_RecordType_Autoupdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Agriculture</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Opp RecordType Autoupdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage</fullName>
        <field>StageName</field>
        <literalValue>Business Leader Review</literalValue>
        <name>Opportunity Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Past_due_Notes</fullName>
        <description>This rule updates the notes requiring the BSE to make entries based on contract renewal efforts</description>
        <field>Notes__c</field>
        <formula>&quot;Send email to Sales Lead and Business Solutions Director&quot;</formula>
        <name>Past due Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled_field_update</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Recall</literalValue>
        <name>Recalled field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejection_field_update</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejection field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Expired_notice_sent</fullName>
        <field>Expired_notice_sent__c</field>
        <name>Reset Expired notice sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Amount_from_Lead_Expected_Revenue</fullName>
        <field>Amount</field>
        <formula>Lead_Expected_Revenue__c</formula>
        <name>Set Amount from Lead Expected Revenue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_to_Pending</fullName>
        <field>Transactional_Approval__c</field>
        <literalValue>Pending Approval</literalValue>
        <name>Set Approval to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submission_Step</fullName>
        <field>Deal_approvals__c</field>
        <literalValue>Pending</literalValue>
        <name>Submission Step</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Transactional_Approved</fullName>
        <field>Transactional_Approval__c</field>
        <literalValue>Approved</literalValue>
        <name>Transactional Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Transactional_Not_Approved</fullName>
        <field>Transactional_Approval__c</field>
        <literalValue>Not Approved</literalValue>
        <name>Transactional Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Transactional_Recalled</fullName>
        <field>Transactional_Approval__c</field>
        <name>Transactional Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Undo_Checkbox</fullName>
        <field>Show_On_Forecast_Report__c</field>
        <literalValue>0</literalValue>
        <name>Undo Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contracted</fullName>
        <field>SBQQ__Contracted__c</field>
        <literalValue>1</literalValue>
        <name>Update Contracted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Z_Test_field_update</fullName>
        <description>This is a test on training date - 3/2/2019</description>
        <field>Notes_Special_Instructions__c</field>
        <formula>&quot;Z-Test update note&quot;</formula>
        <name>Z-Test field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ordered</fullName>
        <field>SBQQ__Ordered__c</field>
        <literalValue>1</literalValue>
        <name>ordered</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Bid Submitted 120 Days Ago</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Bid / RFP Submitted</value>
        </criteriaItems>
        <description>From Geoverse--Bid Submitted 120 days ago, assign task to deal owner to follow-up with customer or update stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Bid_RFP_Submitted_120_Days_Ago</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.Date_Stage_Changed_Geo__c</offsetFromField>
            <timeLength>120</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Bid Submitted 30 Days Ago</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Bid / RFP Submitted</value>
        </criteriaItems>
        <description>From Geoerse--Bid Submitted 30 days ago, assign task to deal owner to follow-up with customer or update stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Bid_RFP_Submitted_30_Days_Ago</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.Date_Stage_Changed_Geo__c</offsetFromField>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Bid Submitted 75 Days Ago</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Bid / RFP Submitted</value>
        </criteriaItems>
        <description>From Geoverse--Bid Submitted 75 days ago, assign task to deal owner to follow-up with customer or update stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Bid_RFP_Submitted_75_Days_Ago</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Opportunity.Date_Stage_Changed_Geo__c</offsetFromField>
            <timeLength>75</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Billing Start Date</fullName>
        <actions>
            <name>Billing_Start_Date_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Billing_Start_Date__c</field>
            <operation>greaterThan</operation>
            <value>1/1/1950</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.SubStage_Geo__c</field>
            <operation>equals</operation>
            <value>Revenue Generating</value>
        </criteriaItems>
        <description>From Geoverse</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Deal Notification to Eng</fullName>
        <actions>
            <name>Deal_Stage_Changed_to_Closed_Email_Sent_to_Eng</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Entity__c</field>
            <operation>equals</operation>
            <value>Commnet,WestNet</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed- Workbook Not Approved,Closed - Not Bid,Closed by Customer,Closed- Not Pursuing</value>
        </criteriaItems>
        <description>From Geoverse--Send email notification to engineering that deal was closed and not won.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Closed Won - Contracted%2FOrdered</fullName>
        <actions>
            <name>Update_Contracted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ordered</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Completed,Closed - Renewed,Signed,Invoiced,Implemented</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>ATNi - CPQ Deal,Partner - CPQ Deal</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contracts Expiring Rule</fullName>
        <actions>
            <name>Notify_team_of_pending_Contract_Expiration</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( ISCHANGED( Expired_notice_sent__c ), NOT(ISBLANK(Expired_notice_sent__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contracts Expiring in 45 days Rule</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.SubStage_Geo__c</field>
            <operation>equals</operation>
            <value>Revenue Generating</value>
        </criteriaItems>
        <description>From Geoverse--rule to send email alerts when a contract is expiring within 45 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Contract_Expiring_within_45_Days_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Contract_Expiration_Date__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Field_Updates</fullName>
        <actions>
            <name>Follow_up</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Amount</field>
            <operation>greaterThan</operation>
            <value>&quot;USD 1,000&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opp RecordType Autoupdate</fullName>
        <actions>
            <name>Opp_RecordType_Autoupdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Master_Record_Type__c</field>
            <operation>equals</operation>
            <value>Agriculture</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Past_due</fullName>
        <actions>
            <name>Past_due_opportunities_email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Follow_up</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>This workflow is to trigger a nudge to the BSE for past due opportunities</description>
        <formula>CloseDate=DATE(2019,02,28)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Past_due_Notes</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Req for Quote Sent to Eng Notification</fullName>
        <actions>
            <name>Deal_Stage_Changed_to_Req_for_Quote_Sent_to_Eng</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Entity__c</field>
            <operation>equals</operation>
            <value>Commnet,WestNet</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Qualified Opportunity</value>
        </criteriaItems>
        <description>From Geoverse--Req for Quote Sent to Eng for WestNet and Commnet</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Req for Quote Sent to Eng Notification - NTUAW</fullName>
        <actions>
            <name>Deal_Stage_Changed_to_Req_for_Quote_Sent_to_Eng_NTUAW</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Entity__c</field>
            <operation>equals</operation>
            <value>NTUAW</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Qualified Opportunity</value>
        </criteriaItems>
        <description>From Geoverse--Req for Quote Sent to Eng for NTUAW</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reset Expired notice sent date</fullName>
        <actions>
            <name>Reset_Expired_notice_sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Contract End Date is updated this will clear the Expired notice sent date field.</description>
        <formula>AND( ISCHANGED(Contract_Expiration_Date__c), NOT(ISBLANK(PRIORVALUE(Contract_Expiration_Date__c))) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Viya set initial Amount on Deal</fullName>
        <actions>
            <name>Set_Amount_from_Lead_Expected_Revenue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For Leads that are converted for VIYA, populate the Amount field with the value entered into the Lead Expected Revenue field if it is not blank.</description>
        <formula>AND( RecordType.DeveloperName = &quot;VIYA&quot;, NOT(ISBLANK( Lead_Expected_Revenue__c )) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Bid_RFP_Submitted_120_Days_Ago</fullName>
        <assignedToType>owner</assignedToType>
        <description>Bid Submitted 120 days ago, assign task to deal owner to follow-up with customer or update stage.</description>
        <dueDateOffset>122</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Date_Stage_Changed_Geo__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Bid / RFP Submitted 120 Days Ago</subject>
    </tasks>
    <tasks>
        <fullName>Bid_RFP_Submitted_30_Days_Ago</fullName>
        <assignedToType>owner</assignedToType>
        <description>Task for Deal Owner to follow-up with customer or update stage.</description>
        <dueDateOffset>31</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Date_Stage_Changed_Geo__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Bid / RFP Submitted 30 Days Ago</subject>
    </tasks>
    <tasks>
        <fullName>Bid_RFP_Submitted_75_Days_Ago</fullName>
        <assignedToType>owner</assignedToType>
        <description>Bid/RFP Submitted Stage for 75 Days. Review and upstage as needed.</description>
        <dueDateOffset>76</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Date_Stage_Changed_Geo__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Bid / RFP Submitted 75 Days Ago</subject>
    </tasks>
    <tasks>
        <fullName>Follow_up</fullName>
        <assignedToType>owner</assignedToType>
        <description>This task is due 3 days after receiving the email</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Follow up on expired contract</subject>
    </tasks>
</Workflow>
