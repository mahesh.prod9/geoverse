@isTest
Public class RG_ExportMultiSheetCtrl_Tc{
    static testMethod void TestData() {
        RecordType rt = [SELECT id,Name 
                             FROM RecordType 
                             WHERE SobjectType='Account' AND Name='Company'];
String values = 'A,B,C';

         Account acc = new Account();
        acc.Name = 'test';
        acc.recordTypeId = rt.id;
        acc.Technology__c = values;
        insert acc;
        
        RecordType rt1 = [SELECT id,Name 
                             FROM RecordType 
                             WHERE SobjectType='Opportunity' AND Name='Company'];
        Opportunity op = new Opportunity();
        op.Name = 'test';
        op.recordTypeId = rt1.id;
        op.AccountId = acc.id;
        op.stageName = 'Negotiation/Review';
        op.closedate = system.today();
        op.Identifier__c = '1234';
        op.Account__c = '1234';
        op.New_Existing_Deal__c = 'New';
        op.Incremental_MRC__c = '1';
        op.Incremental_MB__c = '1';
        op.Unit__c = 'Enterprise 1';
        op.Mobiles_Unit_Count__c = 1;
        op.Technology__c = values;
         insert op;
         ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(op);
         ApexPages.currentPage().getParameters().put('Id',op.id);
        RG_ExportMultiSheetCtrl rg = new RG_ExportMultiSheetCtrl(sc);
        RG_ExportMultiSheetCtrl rg1 = new RG_ExportMultiSheetCtrl();
        rg1.getOpp1();
        rg1.getOpp2();
        String s = rg1.xlsHeader;
       
    }    
}