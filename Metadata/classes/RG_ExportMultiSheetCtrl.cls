public with sharing class RG_ExportMultiSheetCtrl {

    public RG_ExportMultiSheetCtrl(ApexPages.StandardController controller) {

    }

    
    private transient Opportunity opp1 = new Opportunity();
    private transient Opportunity opp2 = new Opportunity();
    Public string recordId {get;set;}
     public String xlsHeader {
            get {
                String strHeader = '';
                strHeader += '<?xml version="1.0"?>';
                strHeader += '<?mso-application progid="Excel.Sheet"?>';
                return strHeader;
            }
        }
      
    
    public RG_ExportMultiSheetCtrl(){
        
        recordID = ApexPages.CurrentPage().getparameters().get('id');
        opp1 = [SELECT Id,Name,Opportunity.Account.RecordType.Name,Account.Name FROM Opportunity where id=: recordID];
        opp2 = [SELECT Id,Name, Opportunity.Account.RecordType.Name,Account.Name FROM Opportunity where id=: recordID];
    }
    
    public Opportunity getOpp1(){
        return Opp1;
    }
    
    public Opportunity getOpp2(){
        return Opp2;
    }
}